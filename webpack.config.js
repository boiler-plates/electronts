const mainConfig = require("./config/webpack.main");
const renderConfig = require("./config/webpack.render");
const allRules = require("./config/webpack.rules");
const allPlugins = require("./config/webpack.plugins");
const { modes } = require("./config.json");

const paths = require("./helpers/path");

module.exports = function (env) {
  const isDev = !!env.development;
  const target = env.target;
  const isMain = target == "main";
  const configFactory = isMain ? mainConfig : renderConfig;

  return {
    context: paths.src(""),
    mode: isDev ? modes.dev : modes.prod,
    output: {
      filename: "[name].js",
      path: paths.dist(""),
    },
    module: {
      rules: allRules(isMain),
    },
    plugins: isMain ? allPlugins(isMain) : allPlugins(isMain).filter(Boolean),
    resolve: {
      extensions: [".js", ".jsx", ".ts", ".tsx", ".json", ".css", ".scss"],
      alias: paths.alias,
    },
    ...configFactory(isDev),
  };
};
