const { main } = require("../package.json");

module.exports = function createMainConfig(isDev) {
  return {
    target: "electron-main",
    entry: {
      "main-process": `./${main.split(".")[0]}.ts`,
    },
  };
};
