const { devServer } = require("../config");
const paths = require("../helpers/path");

module.exports = function createRenderConfig(isDev) {
  return {
    target: "electron-renderer",
    devtool: isDev ? "source-map" : undefined,
    entry: {
      "render-process": "./render-process.tsx",
    },
    devServer: isDev
      ? {
          ...devServer,
          contentBase: paths.dist(""),
        }
      : undefined,
  };
};
