function base(isDev) {
  return [];
}

function main(isDev) {
  return [
    {
      test: /\.(ts|js)$/,
      exclude: /node_modules/,
      use: {
        loader: "babel-loader",
        options: {
          presets: ["@babel/preset-typescript"],
        },
      },
    },
  ];
}

function render(isDev) {
  return [
    {
      test: /\.css$/,
      use: ["style-loader", "css-loader"],
    },
    {
      test: /\.(jpg|jpeg|png|gif|svg)$/,
      use: [{ loader: "url-loader?limit=100000" }],
    },
    {
      test: /\.(js|jsx|ts|tsx)$/,
      exclude: /node_modules/,
      use: {
        loader: "babel-loader",
        options: {
          presets: ["@babel/preset-typescript", "@babel/preset-react"],
          plugins: [
            "@babel/plugin-proposal-class-properties",
            "@babel/plugin-transform-runtime",
            isDev && require.resolve("react-refresh/babel"),
          ].filter(Boolean),
        },
      },
    },
  ];
}

module.exports = function create(isDev) {
  const config = isDev ? main : render;
  return [...base(isDev), ...config(isDev)];
};
