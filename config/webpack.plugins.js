const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const DefinePlugin = require("webpack").DefinePlugin;
const CopyWebpackPlugin = require("copy-webpack-plugin");
const HtmlPlugin = require("html-webpack-plugin");
const ReactRefreshWebpackPlugin = require("@pmmmwh/react-refresh-webpack-plugin");
const webpack = require("webpack");
const { modes, htmlFilePath } = require("../config.json");
const { main } = require("../package.json");

function createBase(isDev) {
  return [];
}

function createMain(isDev) {
  return [
    new CleanWebpackPlugin({
      cleanOnceBeforeBuildPatterns: ["main-process.*.js"],
      cleanOnceBeforeBuildPatterns: [`${main.split(".")[0]}.*.js`],
    }),
    new DefinePlugin({
      ENVIRONMENT: JSON.stringify(isDev ? modes.dev : modes.prod),
    }),
    new CopyWebpackPlugin({
      patterns: [{ from: "package.json", to: "./", context: "../" }],
    }),
  ];
}

function createRender(isDev) {
  return [
    new CleanWebpackPlugin({
      cleanOnceBeforeBuildPatterns: ["**/*", `!${main.split(".")[0]}.*.js`],
    }),
    new HtmlPlugin({
      filename: `${htmlFilePath.entry}.${htmlFilePath.extention}`,
      template: `${htmlFilePath.entry}.${htmlFilePath.extention}`,
      cache: true,
    }),
    isDev && new webpack.HotModuleReplacementPlugin(),
    isDev && new ReactRefreshWebpackPlugin(),
  ];
}

module.exports = function create(isDev) {
  const config = isDev ? createMain : createRender;
  return [...createBase(isDev), ...config(isDev)];
};
