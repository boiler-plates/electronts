import { app, BrowserWindow } from "electron";
import configs from "../config.json";

declare const ENVIRONMENT: String;
const { server, devServer, htmlFilePath, browserWindows } = configs;
const IS_DEV = ENVIRONMENT == "development";
const DEVTOOLS = process.env.npm_config_tools;
const DEFAULT_MENU = process.env.npm_config_menu;
const FRAME = process.env.npm_config_frame ? true : false;
let win: BrowserWindow | null = null;

function createWindow() {
  win = new BrowserWindow({ frame: FRAME, ...browserWindows });
  if (IS_DEV) {
    win.loadURL(`${server.front}://${server.url}:${devServer.port}`);
    DEVTOOLS && win.webContents.openDevTools();
  } else {
    win.loadFile(`${htmlFilePath.entry}.${htmlFilePath.entry}`);
  }
  win.on("closed", () => {
    win = null;
  });
}

app.on("ready", () => {
  createWindow();
});

app.on("window-all-closed", () => {
  if (process.platform !== "darwin") {
    app.quit();
  }
});

app.on("activate", () => {
  if (win === null) {
    createWindow();
  }
});

app.on("browser-window-created", function (_, window) {
  !DEFAULT_MENU && window.setMenu(null);
});
