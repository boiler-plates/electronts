import React from "react";
import DOM from "react-dom";
import App from "core/index";
import config from "../config.json";

DOM.render(<App />, document.getElementById(config.DOMRender.id));
